const factory = require('./aws-service-factory');
const ec2 = factory.ec2();

module.exports = {
    getFromTags: function(config) {

        if ((!config.env) || (!Array.isArray(config.env))) {
            throw Error('Missing env configuration, you must define an array of environments');
        }
        if (!config.tags.env) {
            throw Error('Missing tags.env configuration, you must define the environment tag name');
        }

        // For each tag name there must be a value (except env)
        for (var key in config.tags) {
            if (key == 'env') continue;

            if ((!config.values) || (!config.values[key])) {
                throw Error('Tag key ' + key + ' (' + config.tags[key] + ') does not have a value defined');
            }
        }

        return new Promise(function(resolve, reject) {
            var params = {
                Filters: [
                    {
                        Name: 'instance-state-name',
                        Values: ['running']
                    }
                ]
            };

            for(var key in config.tags) {
                params.Filters.push({
                    Name: 'tag-key',
                    Values: [
                        config.tags[key]
                    ]
                });
            }

            params.Filters.push({
                Name: 'tag:' + config.tags.env,
                Values: config.env
            });

            ec2.describeInstances(params, function(err, data) {
                if (err) { reject(err); return; }

                var reservation, instance, tags, validTags;
                var instances = {};

                for (var e in config.env) {
                    instances[config.env[e]] = [];
                }

                for (var x in data.Reservations) {
                    reservation = data.Reservations[x];
                    for (var y in reservation.Instances) {
                        instance = reservation.Instances[y];

                        tags = {};
                        for(var t in instance.Tags) {
                            tags[instance.Tags[t].Key] = instance.Tags[t].Value.split(',');
                        }

                        var validTags = true;

                        // Ensure tags contain the configured tag value
                        for (var key in config.tags) {
                            if (key == 'env') continue;

                            if ((!tags[config.tags[key]]) || (!config.values[key])) {
                                validTags = false; break;
                            }
                            if (tags[config.tags[key]].indexOf(config.values[key]) == -1) {
                                validTags = false; break;
                            }
                        }

                        if (!validTags) {
                            continue;
                        }

                        // Create a structure by environment
                        for (var e in config.env) {
                            if (tags[config.tags.env].indexOf(config.env[e]) != -1) {
                                if (config.simple) {
                                    instances[config.env[e]].push(instance.PublicIpAddress);
                                } else {
                                    instances[config.env[e]].push({
                                        id: instance.InstanceId,
                                        host: instance.PublicDnsName || instance.PublicIpAddress,
                                        ip: {
                                            public: instance.PublicIpAddress,
                                            private: instance.PrivateIpAddress
                                        },
                                        dns: {
                                            public: instance.PublicDnsName,
                                            private: instance.PrivateDnsName
                                        },
                                        type: instance.InstanceType,
                                        time: instance.LaunchTime,
                                        tags: tags
                                    });
                                }
                            }
                        }
                    }
                }

                resolve(instances);
            });
        });
    }
};
