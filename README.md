# shipit-ec2

shipit-ec2 is a simple async task to obtain a list of aws ec2 instances for use in configuring shipit-cli.

While at the time of writing, this module could be used elsewhere, it is written for the purpose of shipit-cli and may create dependencies in the future depending on desired features.

## Usage

### Example `shipitfile.js`

```javascript
var ec2 = require('shipit-ec2');

module.exports = function (shipit) {

    return ec2.getFromTags({
        env: ['prod'],
        tags: {
            env: 'shipit.env',
            app: 'shipit.app'
        },
        values: {
            app: 'my-app'
        },
        simple: true
    }).then(function(instances) {

        shipit.initConfig({
            default: {
                key: '/path/to/key.pem'
            },
            prod: {
                servers: instances.prod.map(function(instance) { return 'ubuntu@' + instance; })
            }
        });

        shipit.task('pwd', function () {
            return shipit.remote('pwd');
        });

    });
};
```

## Options

### env

Type: Array<String>

The environments we wish to obtain servers for. This will be returned as properties of the resolved `instances` variable (see the above example).

### tags

Type: Object<String, String>

An object defining the tag names used with ec2 instances.

With exception of `env` which defines the tag name for the `env` configuration values, the keys should match the values defined within the `values` configuration.

### values

Type: Object<String, String>

An object defining the corresponding values for the tags defined with the `tags` configuration.

### simple

Type: Boolean

If true, the list of resolved servers (see `instances` variable in the above example) will be a simple structure of environment name against an array of servers. If false, a more complex object structure giving more details about the instance.

## AWS Configuration

Aws configuration, should reside within the file `aws.json` in the current working directory, alongside `shipitfile.js`.

The IAM user should have list privileges for ec2 instances.

``` json
{
    "accessKeyId": "<access key>",
    "secretAccessKey": "<secret access key>",
    "region": "<region>"
}
```

The aws credentials may be omitted from this file, in favour of the environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
